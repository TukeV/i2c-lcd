# I2C-LCD #

## What is it? ##

This repository has a happy little python class, which controls the HD44780 compliant 2x16 lcd display over MCP23017 gpio expander. This frees more GPIOs for other purposes and is to IMHO easier setup than hooking up the lcd directly to the Raspberry Pi (or similair).

### Hardware ###

My setup required the following parts:

* Raspberry Pi (I have Rev 2).
* MCP23017 GPIO expander.
* 2x16 LCD screen (hd44780 compliant).
* 2 x 10k Ohm rotary potentiometers.

The Repository has a Fritzing diagram of the wiring.

![mcplcd_bb.png](https://bitbucket.org/repo/xz6KgX/images/789585730-mcplcd_bb.png)

Few notes about the wiring:

* The MCP and LCD operate with different input voltage. The MCP uses 3.3V and the LCD 5V. Make sure you don't connect MCP to 5 volts.
* The MCPs default i2c address is 0x20, but it can be changed by changing how brown wires are connected.
* Potentiometers can be replaced with regular resistors, but it's more handy to use potentiometer than trying several resistors.
* Connecting the 4 data pins this way simplifies the data signaling since I didn't have to test for each GPIO that do I need to turn it on or not (see the raspberry-spy example from link below).


### Usage ###

Make sure that when you are using this code, that you have enabled the I2C interface and that the python is authored to use it.

Otherwise the class is fairly straightforward to use...


```
#!python

>>> from i2clcd import I2C_Lcd
>>> lcd = I2C_Lcd()
>>> lcd.write_line("Hello World!", 0)
>>> lcd.write_line("Goodbye world!", 1)
>>> lcd.clear()
>>> lcd.display_control(False, False, False)
>>> exit()
```


### Issues ###

* At the moment the writing the messages take more time than it could.
* You cannot create or write your own symbols unless you know how what bytes to write.
* Most of the variables are hard coded! Shame on me!
* No proper cleanup after deletion. It's easier to just pull the plug from the LCD.
* The class doesn't check what pins are already in use which makes you unable to use the other MCPs pins without small modifications to _send_nibble and _lcd_toggle_enable functions (read the GPIO registers and do *logical or* between current status and command bits).


### Links I found useful while creating this. ###

[Raspberry-spy.co.uk: Enabling The I2C Interface On The Raspberry Pi](http://www.raspberrypi-spy.co.uk/2014/11/enabling-the-i2c-interface-on-the-raspberry-pi/)

[MCP23017 Datasheet(PDF)](http://ww1.microchip.com/downloads/en/DeviceDoc/21952b.pdf)

[Raspberry-spy.co.uk: How To Use A MCP23017 I2C Port Expander With The Raspberry Pi ](http://www.raspberrypi-spy.co.uk/2013/07/how-to-use-a-mcp23017-i2c-port-expander-with-the-raspberry-pi-part-1/)

[HD44780 Datasheet(PDF)](https://www.sparkfun.com/datasheets/LCD/HD44780.pdf)

[HD44780 Wikipedia article](https://en.wikipedia.org/wiki/Hitachi_HD44780_LCD_controller)

[Raspberry-spy.co.uk: 16×2 LCD Module Control Using Python *without the expander*](http://www.raspberrypi-spy.co.uk/2012/07/16x2-lcd-module-control-using-python/)

[Donald Weiman LCD Initialization](http://web.alfredstate.edu/weimandn/lcd/lcd_initialization/lcd_initialization_index.html)

I found his flowchart incredibly useful when trying to demystify the initialization process. His other work looks also useful if someone wants to port this to support other LCD sizes.