#!/usr/bin/env python
# -*- coding: utf-8 -*-
import smbus
import time


class I2C_Lcd():
    """A class which uses MCP23017 to control hd44780 compliant lcd screen. Tested only with 2x16 lcd screen."""
    # MCP Constants
    
    # Default i2c address
    MCP = 0x20 
    # GPIO direction registers
    IODIRA = 0x00 
    IODIRB = 0x01
    # GPIO status registers
    GPIOA = 0x12
    GPIOB = 0x13
    
    #LCD Constants
    LCD_WIDTH = 16    # Maximum characters per line
    CHAR = True
    CMD = False
    LCD_LINE_1 = 0x80 # LCD RAM address for the 1st line
    LCD_LINE_2 = 0xC0 # LCD RAM address for the 2nd line
    LCD_LINE = [0x80, 0xC0]
    WAIT = 0.005
    
    CMD_ON = 0x40
    CMD_OFF = 0x00
    CHAR_OFF = 0x80
    CHAR_ON = 0xc0
      
    def __init__(self):
        time.sleep(0.2)
	# Initialise the MCP
        self.bus = smbus.SMBus(1)
        self.bus.write_byte_data(self.MCP, self.IODIRB, 0)
        self.bus.write_byte_data(self.MCP, self.IODIRA, 0)
        
	# Set to 4 bit mode
        self._lcd_byte(0x33, False)
        self._lcd_byte(0x32, False)
        self._lcd_byte(0x2c, False)      
        
        # Set entry mode, clear display and put cursor to home.
        self.display_control(False, False, False)
        self.clear()
        self.entry_mode(True, False)        
        self.display_control(True, True, True)
        self.clear()
        self.home()


    def _lcd_byte(self, bits, mode):
        """Splits byte into two nibbles and sends them."""
        high = bits >> 4 # get higher 4 bits
        low = bits & 0x0f # get lower 4 bits
        self._send_nibble(high, mode) 
        self._send_nibble(low, mode)


    def _send_nibble(self, data, rs):
        """Sends the nibble and clears the output register (GPIOB)"""
        self.bus.write_byte_data(self.MCP, self.GPIOB, data) 
        self._lcd_toggle_enable(rs)
        self.bus.write_byte_data(self.MCP, self.GPIOB, 0)

 
    def _lcd_toggle_enable(self, rs):
        """Flicks the enabled register to send the bits."""
        # The register status depends on are we sending character
        # or a lcd command.
        on =  self.CHAR_ON if rs else self.CMD_ON
        off =  self.CHAR_OFF if rs else self.CMD_OFF
        # To make sure that rs bit is correct, we have to set it
        # before flicking the enabled.
        self.bus.write_byte_data(self.MCP, self.GPIOA, off)
        self.bus.write_byte_data(self.MCP, self.GPIOA, on)
	time.sleep(self.WAIT)
        self.bus.write_byte_data(self.MCP, self.GPIOA, off)
	time.sleep(self.WAIT)  
 
    def home(self):
        """Sets cursor to home."""
        self._send_nibble(0, False)
        self._send_nibble(2, False)       
 
    def clear(self): 
        """Clears the display"""     
        self._send_nibble(0, False)
        self._send_nibble(1, False)

    def display_control(self, display=True, cursor=True, blink=True):
        """Sends display control command.
        For more information refer to HD447280 datasheet or wikipedia article."""
        cmd = 0x08 | display << 2 | cursor << 1 | blink 
        self._lcd_byte(cmd, self.CMD)

    def write_line(self, message, line):
        """
        Writes given message to line (0 for first line, 1 for second line).
        If the message is too long, it is truncated to maxium size.
        """
        message = message.ljust(16, " ")
        line = self.LCD_LINE[line] 
        self._lcd_byte(line, False)    
        for i in message: 
            self._lcd_byte(ord(i), True)
    
    def cursor_shift(self, sc, s_dir):
        """
        Sends the cursor/display shift command.
        sc marks the cursor move or display shift bit.
        s_dir marks shift direction.
        For more information refer to HD447280 datasheet or wikipedia article."""
        cmd = 0x10 | sc << 3 | s_dir << 2
        self._lcd_byte(cmd, self.CMD)

    def entry_mode(self, c_dir, shift):
        """
        Sends entrymode command, where c_dir marks the cursor move direction and shift for shift on/off.
        For more information refer to HD447280 datasheet or wikipedia article."""
        cmd = 0x04 | c_dir << 1 | shift
        self._lcd_byte(cmd, self.CMD)
